using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace GGR_BackupTool
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string username = Environment.UserName;
            string filepath = "C:/Users/" + username + "/AppData/Local/GalGunReturns/";
            if (Directory.Exists(filepath))
                filepath = Directory.GetDirectories(filepath)[0];
            else
            {
                MessageBox.Show("No save game folder for the active user exists on this machine.");
                return;
            }

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new GGR_BackupTool_MainForm());
        }
    }
}
