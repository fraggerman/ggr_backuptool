﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace GGR_BackupTool
{
    public partial class GGR_BackupTool_MainForm : Form
    {
        string filepath;
        FileNameDialog fileNameDialog;

        public GGR_BackupTool_MainForm()
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            string username = Environment.UserName;
            filepath = Directory.GetDirectories("C:/Users/" + username + "/AppData/Local/GalGunReturns/")[0];

            if (Directory.Exists(Environment.CurrentDirectory + "/backups"))
                foreach (string filename in Directory.GetFiles(Environment.CurrentDirectory + "/backups"))
                {
                    listView1.Items.Add(Path.GetFileName(filename));
                }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (File.Exists(filepath + "/SaveData.sav"))
            {
                fileNameDialog = new FileNameDialog();
                fileNameDialog.FormClosed += new FormClosedEventHandler(FileFormClosed);
                fileNameDialog.Show();
            }
            else
            {
                MessageBox.Show("No save game for the active user exists on this machine.");
                return;
            }
        }

        private void FileFormClosed(object sender, FormClosedEventArgs e)
        {
            string filename = fileNameDialog.Filename;
            Directory.CreateDirectory(Environment.CurrentDirectory + "/backups");
            File.Copy(filepath + "/SaveData.sav", Environment.CurrentDirectory + "/backups/" + filename + ".backup");
            listView1.Items.Add(Path.GetFileName(filename) + ".backup");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the selected backup?", "Delete Backup", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string filename = listView1.SelectedItems[0].Text;
                File.Delete(Environment.CurrentDirectory + "/backups/" + filename);
                for(int i = 0; i < listView1.Items.Count; i++)
                {
                    if(listView1.Items[i].Text == filename)
                    {
                        listView1.Items.RemoveAt(i);
                        return;
                    }
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to overwrite your savegame with the selected backup?", "Restore Backup", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string filename = listView1.SelectedItems[0].Text;
                File.Delete(filepath + "/SaveData.sav");
                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    if (listView1.Items[i].Text == filename)
                    {
                        //copy here
                        File.Copy(Environment.CurrentDirectory + "/backups/" + filename, filepath + "/SaveData.sav");
                        return;
                    }
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                return;
            }
        }
    }
}
