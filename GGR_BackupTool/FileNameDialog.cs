﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GGR_BackupTool
{
    public partial class FileNameDialog : Form
    {
        public string Filename;

        public FileNameDialog()
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            Filename = DateTime.Now.ToString("MM/dd/yyyy");
            textBox1.Text = Filename;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Filename = textBox1.Text;
            Close();
        }
    }
}
